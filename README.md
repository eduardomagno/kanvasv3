# Kanvas

Api desenvolvida para a atividade Kanvas na Kenzie Academy Brasil.
A api foi desenvolvida com django utilizando rest_framework


## Endpoints
#

### POST /api/accounts/
> cria usuários

Body:

criando um estudante:
```json

{
  "username": "student",
  "password": "1234",
  "is_superuser": false,
  "is_staff": false
}
```

Response:
```json
{
  "id": 1,
  "is_superuser": false,
  "is_staff": false,
  "activity_set": [], // esse campo é opcional
  "username": "student"
}
```

criando um facilitador:
```json

{
  "username": "facilitador",
  "password": "1234",
  "is_superuser": false,
  "is_staff": true
}
```

Response:
```json
{
  "id": 2,
  "is_superuser": false,
  "is_staff": true,
  "activity_set": [], // esse campo é opcional
  "username": "facilitator"
}
```

criando um instrutor:
```json

{
  "username": "instrutor",
  "password": "1234",
  "is_superuser": true,
  "is_staff": true
}
```

Response:
```json
{
  "id": 3,
  "is_superuser": true,
  "is_staff": true,
  "activity_set": [], // esse campo é opcional
  "username": "instructor"
}
```

### POST /api/login/
> faz autenticação

Body:
```json
{
	"username": "instrutor101",
	"password": "12345"
}
```

Response:
```json
{
  "token": "dfd384673e9127213de6116ca33257ce4aa203cf"
}
```

### POST /api/courses/
> cria curso (user: instrutor apenas)

Header:
```json
Header -> Authorization: Token <token-do-instrutor>
```

Body:
```json
{
	"name": "Javascript 101"
}
```

Response:
```json
  "id": 1,
  "name": "Javascript 101",
  "user_set": []
}
```

### PUT /api/courses/registrations/
> matricula estudantes num determinado curso (user: instrutor apenas)

Header:
```json
Header -> Authorization: Token <token-do-instrutor>
```

Body:
```json
{
	"course_id": 1,
	"user_ids": [1, 2, 7]
}
```

Response:
```json
{
  "id": 1,
  "name": "Javascript 101",
  "user_set": [
    {
      "id": 1,
      "is_superuser": false,
      "is_staff": false,
      "username": "luiz"
    },
    {
      "id": 7,
      "is_superuser": false,
      "is_staff": false,
      "username": "isabela"
    },
    {
      "id": 2,
      "is_superuser": false,
      "is_staff": false,
      "username": "raphael"
    }
  ]
}

```

Erro na requisição caso não seja passado o token:
```json
{
  "detail": "You do not have permission to perform this action."
}
```

### GET /api/courses/
>  lista cursos e alunos matriculados (sem autenticação)

Response:
```json
[
  {
    "id": 1,
    "name": "Javascript 101",
    "user_set": [
      {
        "id": 1,
        "is_superuser": false,
        "is_staff": false,
        "username": "luiz"
      }
    ]
  },
  {
    "id": 2,
    "name": "Python 101",
    "user_set": []
  }
]
```

### POST /api/activities/
>cria atividade do estudante (sem nota, user: estudante)

Header:
```json
Header -> Authorization: Token <token-do-estudante> 
```

Body:
```json
{
	"repo": "gitlab.com/cantina-kenzie",
	"user_id": 7,
	"grade": 10
}
```

Response:
```json
{
  "id": 6,
  "user_id": 7,
  "repo": "gitlab.com/cantina-kenzie",
  "grade": null
}
```

### PUT /api/activities/
> edita atividade - atribui nota - (user: facilitador e instrutor)

Header:
```json
Header -> Authorization: Token <token-do-instrutor> ou <token-do-facilitador>
```

Body:
```json
{
  "id": 6,
  "repo": "gitlab.com/cantina-kenzie",
  "user_id": 7,
  "grade": 10
}
```

Response:
```json
{
  "id": 6,
  "user_id": 7,
  "repo": "gitlab.com/cantina-kenzie",
  "grade": 10
}
```

### GET /api/activities/
> lista atividades do estudante (user: estudante)

Header:
```json
Header -> Authorization: Token <token-do-estudante> 
```

Response:
```json

  {
    "id": 1,
    "user_id": 1,
    "repo": "github.com/luiz/cantina",
    "grade": null
  },
  {
    "id": 2,
    "user_id": 1,
    "repo": "github.com/hanoi",
    "grade": null
  },
  {
    "id": 3,
    "user_id": 1,
    "repo": "github.com/foodlabs",
    "grade": null
  },
]
```

### GET /api/activities/
>  lista todas as atividades de todos os estudantes (user: facilitador e instrutor)

Header:
```json
Header -> Authorization: Token <token-do-instrutor> ou <token-do-facilitador>
```

Response:
```json
[
  {
    "id": 1,
    "user_id": 1,
    "repo": "github.com/luiz/cantina",
    "grade": null
  },
  {
    "id": 2,
    "user_id": 1,
    "repo": "github.com/hanoi",
    "grade": null
  },
  {
    "id": 3,
    "user_id": 1,
    "repo": "github.com/foodlabs",
    "grade": null
  },
  {
    "id": 35,
    "user_id": 99,
    "repo": "github.com/kanvas",
    "grade": null
  },
]
```

### GET /api/activities/<int:user_id>/
> filtra as atividades por user_id (user: facilitador e instrutor)

Header:
```json
Header -> Authorization: Token <token-do-instrutor> ou <token-do-facilitador>
```

Response:
```json
  {
    "id": 1,
    "user_id": 1,
    "repo": "github.com/luiz/cantina",
    "grade": null
  },
  {
    "id": 2,
    "user_id": 1,
    "repo": "github.com/hanoi",
    "grade": null
  },
  {
    "id": 3,
    "user_id": 1,
    "repo": "github.com/foodlabs",
    "grade": null
  },
]
```
Se não houver usuário:
```json
{ 
  "detail": "Invalid user_id." 
}
```
