from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from django.core import exceptions
from app.services.create_user import create_user
from app.services.activities import create_activity, show_activities_student, show_activities_staff , update_grade
from app.services.courses import show_courses, create_course, update_course
from app.serializers import UserSerializer
from app.services.login import login
import json

from django.contrib.auth import authenticate
from django.contrib.auth.models import User



## CHECK
class LoginView(APIView):
			
	def post(self, request):
		data = request.data
		# TODO
		# fazer um serializer para o token, se sobrar tempo
		return Response({'token': login(data)}, status=200)

## CHECK
class AccountView(APIView):

	def post(self, request):
		data = request.data
		print(data)
		# TODO 
		# adicionar a create_user e retorna o seu retorno
		return Response(create_user(data), status=201)




class CourseView(APIView):

	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAuthenticated]

	def get(self, request):
		return Response(show_courses(), 201)
	
	def post(self, request):
			user = UserSerializer(request.user)
			courseData = request.data
			
			if user['is_superuser'].value == False:
				return Response({"detail": "You do not have permission to perform this action."}, 403)
				
			return Response(create_course(courseData), 201)
			



class CourseRegistrationView(APIView):

	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAuthenticated]

	def put(self, request):
            user = UserSerializer(request.user)
            courseData = request.data

            if user['is_superuser'].value == False:
                return Response({"detail": "You do not have permission to perform this action."}, 403)
            
            return Response(update_course(courseData), 201)

class ActivitiesView(APIView):

	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAuthenticated]

	def get(self, request, user_id=''):
		activityData = request.data
		user = UserSerializer(request.user)
		if user['is_superuser'].value == True or user['is_staff'].value == True:

			if user_id == '':
				return Response(show_activities_staff(activityData))
			
			try:
				user = User.objects.get(id=int(user_id))
			except exceptions.ObjectDoesNotExist:
				return Response({ "detail": "Invalid user_id." }, 404)

			return Response(show_activities_student(int(user_id)), 201)

		return Response(show_activities_student(user['id'].value), 201)

	def post(self, request):
		activityData = request.data
		user = UserSerializer(request.user)
		
		if user['is_superuser'].value == True or user['is_staff'].value == True:
			return Response({"detail": "You do not have permission to perform this action."}, 403)
		

		return Response(create_activity(activityData))
	

	def put(self, request):
		activityData = request.data
		user = UserSerializer(request.user)

		if user['is_superuser'].value == True or user['is_staff'].value == True:
			return Response(update_grade(activityData), 201)
		
		
		return Response({"detail": "You do not have permission to perform this action."}, 403)