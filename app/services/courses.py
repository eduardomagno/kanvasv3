from app.models import Course
from app.serializers import CourseSerializer

def show_courses():
        
    courses = Course.objects.all()
    serializer = CourseSerializer(courses, many=True)
    return serializer.data

def create_course(courseData):
    #create a new course
    new_course = Course.objects.create(name=courseData['name'])
    new_course.save()

    serializer = CourseSerializer(new_course)
    return serializer.data

def update_course(courseData):
    
    course = Course.objects.get(id=courseData['course_id'])
    course.user_set.clear()
    for user_id in courseData['user_ids']:
        course.user_set.add(user_id)

    serializer = CourseSerializer(course).data

    return serializer