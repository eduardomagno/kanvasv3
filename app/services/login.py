
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
def login(data):
    
    user = authenticate(username=data['username'], password=data['password'])
    if user != None:
        token = Token.objects.get(user=user)
        
        return token.key
    
    return 'Username or Password are invalid. Try again.'
    ...