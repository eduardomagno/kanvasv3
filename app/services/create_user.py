from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from app.serializers import UserSerializer

def create_user(data):

    new_user = User.objects.create_user(
        username=data['username'], 
        password=data['password'], 
        is_staff=data['is_staff'], 
        is_superuser=data['is_superuser']
    )
    new_user.save()

    token = Token.objects.create(user=new_user)

    token.save()
    serializer = UserSerializer(new_user)
    return serializer.data