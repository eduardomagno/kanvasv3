from app.models import Activity
from app.serializers import ActivitySerializer
from django.contrib.auth.models import User


def create_activity(activityData):

    user = User.objects.get(id=activityData['user_id'])
    activity = Activity.objects.create(repo=activityData['repo'], user_id=user)
    activity.save()

    serializer = ActivitySerializer(activity).data
    return serializer
    ...

def update_grade(activityData):
    
    activity = Activity.objects.get(id=activityData['id'])
    activity.grade = activityData['grade']
    activity.save()

    serializer = ActivitySerializer(activity).data
    return serializer




def show_activities_staff(activityData):
    activities = Activity.objects.all()
    serializer = ActivitySerializer(activities, many=True).data
   
    return serializer

def show_activities_student(user_id):
    activities = Activity.objects.filter(user_id=user_id)
    serializer = ActivitySerializer(activities, many=True).data
    
    return serializer