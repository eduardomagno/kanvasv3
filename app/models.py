from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Course(models.Model):
    name = models.CharField(max_length=128)
    user_set = models.ManyToManyField(User)

class Activity(models.Model):
    repo = models.CharField(max_length=128)
    grade = models.IntegerField(null=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='activity')