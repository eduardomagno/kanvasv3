from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Course, Activity

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id', 
            "is_superuser",
            "is_staff",
            "username"
        ]

class CourseSerializer(serializers.ModelSerializer):

    user_set = UserSerializer(many=True)

    class Meta:
        model = Course
        fields = '__all__'


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = '__all__'