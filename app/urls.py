from django.urls import path
from .views import LoginView, AccountView, CourseView, ActivitiesView, CourseRegistrationView

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('accounts/', AccountView.as_view()),
    path('courses/', CourseView.as_view()),
    path('activities/', ActivitiesView.as_view()),
    path('activities/<int:user_id>/', ActivitiesView.as_view()),
    path('courses/registrations/', CourseRegistrationView.as_view()),
]
